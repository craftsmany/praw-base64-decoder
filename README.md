# PRAW Bot to decode Base64 in Reddit comments


Remove "# Optional:" from strings if you want Text output of the things the bot is doing.

Please change everything in square brackets "[ ]" to customize your bot.

List of things to change:

config.py:
- [reddit_username]
- [reddit_password]
- [reddit_api_id]
- [reddit_api_secret]

base64_decoder_bot.py:
- [user-agent]
- [subreddit] // type "all" to monitor all subreddits
- [command] // trigger word
- [search_symbol_start]
- [search_symbol_end]
- [footnote]
